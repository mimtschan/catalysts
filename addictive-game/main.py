import sys
from src import *

with open(sys.argv[1]) as file:
	action = int(sys.argv[2])
	fileInp = file.readline()
	print("in:  " + fileInp)
	board, paths = setupBoard(fileInp, action)
	print("out: " + menu(sys.argv[1], board, paths, action))
