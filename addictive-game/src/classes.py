import copy

class Board:
	def __init__(self, rows, cols):
		self.rows = int(rows)
		self.cols = int(cols)
		self.markers = []
		
		for r in range(0, self.rows):
			self.markers.append([0] * self.cols)
			self.points = []
		
	def markPoints(self, points):
		self.points = sorted(points, key=lambda p: p.color)
	
		for p in points:
			self.markers[p.row - 1][p.col - 1] = p.color
			
	def setMarkers(self, markers):
		self.markers = markers

	def getCoords(self, pos):
		pos = int(pos)
		row = (pos - 1) // self.cols + 1
		col = (pos - 1) % self.cols + 1
		return row, col
	
	def setConnected(self, color):
		pointA = self.points[int(color) * 2 - 1]
		pointB = self.points[int(color) * 2 - 2]
		pointA.status = 1
		pointB.status = 1

class Point:
	def __init__(self, board, row, col, color):
		self.board = board 
		self.color = color
		self.row = row
		self.col = col
		self.status = 2
		
	def performStep(self, direction):
		nrow = self.row
		ncol = self.col
		
		if direction == 78:
			nrow -= 1
		elif direction == 69:
			ncol += 1
		elif direction == 83:
			nrow += 1
		elif direction == 87:
			ncol -= 1
		
		return Point(self.board, nrow, ncol, self.color)
	
	def coordsToPos(self):
		return self.board.rows * (self.row - 1) + self.col
		
	def getDirection(self, other):
		if self.row - 1 == other.row:
			return "N"
		elif self.col + 1 == other.col:
			return "E"
		elif self.row + 1 == other.row:
			return "S"
		elif self.col - 1 == other.col:
			return "W"
	
	def __cmp__(self, other):
		return self.__dict__ == other.__dict__


class Path:
	def __init__(self, color, path, board):
		self.color = color
		self.board = board
		self.points = []
		
		srow, scol = board.getCoords(path[0])
		self.points.append(Point(board, srow, scol, color))
		for i in range(1, len(path)):
			self.points.append(self.points[i - 1].performStep(path[i]))
	
	def backtrack(self, step):
		for i in range(1, step):
			point = self.points[i]
			self.board.markers[point.row - 1][point.col - 1] = 0

	def apply(self):
		numSteps = len(self.points) - 1
		start = self.points[0]
		end = self.points[numSteps]
		
		validEnd = self.board.points[int(self.color) * 2 - 1]
		if validEnd.row == start.row and validEnd.col == start.col:
			validEnd = self.board.points[int(self.color) * 2 - 2]
		
		if start.color != self.color:
			return -1, numSteps
		
		for i in range(1, numSteps):
			point = self.points[i]
			
			if point.row - 1 >= self.board.rows or point.col - 1 >= self.board.cols:
				self.backtrack(i)
				return -1, i
			elif self.board.markers[point.row - 1][point.col - 1] != 0:
				self.backtrack(i)
				return -1, i
			elif point.row == validEnd.row and point.col == validEnd.col:
				self.backtrack(i)
				return -1, i
			else:
				self.board.markers[point.row - 1][point.col - 1] = self.color
		
		if validEnd.row == end.row and validEnd.col == end.col:
			self.board.setConnected(self.color)
			return 1, numSteps
		else:
			#self.backtrack(numSteps)
			return -1, numSteps
	
	def __str__(self):
		string = str(color) + " " + str(points[0].coordsToPos()) + " " + len(points) - 1
		for i in range(0, len(points) - 1):
			string += " " + points[i].getDirection(points[i + 1])

class PartialPath:
	def __init__(self, color, points, board):
		self.color = color
		self.points = points
		self.board = board
		self.end = points[len(points) - 1]
	
	def mergePaths(self, other):
		allPoints = self.points
		
		for i in range (len(other.points) - 1, 0, -1):
			allPoints.append(other.points[i])
		
		return Path(self.color, allPoints, board)
