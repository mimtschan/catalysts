import re
from classes import *

def strToLst(inp):
	if inp.endswith(" "):
		inp = inp[:-1]
	return inp.split(" ")
		
def lstToStr(out):
	return " ".join(str(o) for o in out)

def setupBoard(inp, action):
	if type(inp) is str:
		inp = strToLst(inp)
	
	if action >= 5:
		numBoards = int(inp[0])
		boards = []
		paths = []
		
		offset = 1
		currentInput = []
		
		for i in range(0, numBoards):
			indexNumPoints = offset + 2
			numPoints = int(inp[indexNumPoints])
			
			indexNumPaths = indexNumPoints + numPoints * 2 + 1
			numPaths = int(inp[indexNumPaths])
			
			pathOffset = indexNumPaths + 1
			
			totalPathsLen = 0
			
			for j in range(0, numPaths):
				currentPathLen = int(inp[pathOffset + 2])
				totalPathsLen += currentPathLen + 3
				pathOffset += currentPathLen + 3
				
			for k in range(offset, pathOffset):
				 currentInput.append(inp[k])
			
			offset = pathOffset
			
			newBoard, newPaths = setupBoard(currentInput, 0)
			boards.append(newBoard)
			paths.append(newPaths)
			currentInput = []
		
		return boards, paths
			
			
	board = Board(inp[0], inp[1])
	points = []
	numPoints = int(inp[2])
	paths = []
	numPaths = 0
		
	if action == 1:
		indexPointsEnd = numPoints + 2
	else:
		indexPointsEnd = (2 * numPoints) + 2
	
	if action == 1:
		for n in range(3, indexPointsEnd + 1):
			nrow, ncol = board.getCoords(inp[n])
			points.append(Point(board, nrow, ncol, 0))
	else:
		for n in range(3, indexPointsEnd + 1, 2):
			nrow, ncol = board.getCoords(inp[n])
			points.append(Point(board, nrow, ncol, inp[n + 1]))
	
	board.markPoints(points)
	
	if len(inp) > indexPointsEnd + 1:
		numPaths = int(inp[indexPointsEnd + 1])
	
	indexPathStart = indexPointsEnd + 2
	
	for p in range(0, numPaths):
		currentLength = int(inp[indexPathStart + 2])
		currentPath = []
		
		currentPath.append(inp[indexPathStart + 1])
		regex = re.compile('[NESW]')
		
		for n in range(indexPathStart + 3, indexPathStart + 3 + currentLength):
			if regex.match(inp[n]):
				currentPath.append(ord(inp[n][0]))
		
		paths.append(Path(inp[indexPathStart], currentPath, board))
		indexPathStart += (currentLength + 3)
	
	return board, paths
