from classes import *
from helper import *

def getPositions(board):
	points = []
	for p in board.points:
		points.append(p.row)
		points.append(p.col)
	return lstToStr(points)
