from classes import *
from helper import *

def mdistance(a, b):
	return abs(a.row - b.row) + abs(a.col - b.col)
	
def getDistances(board):
	distances = []
	
	for n in range(0, len(board.points), 2):
		distances.append(mdistance(board.points[n], board.points[n+1]))
		
	return lstToStr(distances)
