from classes import *
from helper import *

def checkPaths(paths):
	checkedPaths = []
	
	for p in paths:
		code, step = p.apply()
		checkedPaths.append(code)
		checkedPaths.append(step)
		
	return lstToStr(checkedPaths)
