from classes import *
from helper import *
from level3 import *

import pygame
import sys

def drawBoard(inpname, board, paths):
	pygame.init()
	
	BLACK = (0,0,0)
	WHITE = (255,255,255)
	
	inpname = inpname.split("/")
	inpname = inpname[len(inpname) - 1].split(".")
	filename = "images/" + inpname[0] + ".png"
	
	print("checking paths...")

	checkPaths(paths)
	
	print("setting up image...")
	
	screen = pygame.display.set_mode([board.cols,board.rows])
	screen.fill(WHITE)

	print("calculating image...")
	
	for i in range(0, board.rows):
		for j in range(0, board.cols):
			if board.markers[i][j] != 0:
				screen.set_at((j, i), BLACK)
	pygame.display.flip()

	print("saving image...")
	
	pygame.image.save(screen, filename)
	pygame.quit()
	
	return "image created"
