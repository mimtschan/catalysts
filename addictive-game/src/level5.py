from classes import *
from helper import *
from level3 import *

def getNeighbors(position):
	neighbors = []
	neighbors.append((position[0] - 1, position[1]))
	neighbors.append((position[0] + 1, position[1]))
	neighbors.append((position[0], position[1] - 1))
	neighbors.append((position[0], position[1] + 1))
	return neighbors

def checkReachability(board, position, visited, goal):
	toCheck = getNeighbors(position)
	
	while len(toCheck) != 0:
		n = toCheck[0]
		toCheck.remove(n)
		if n == goal:
			return True
		elif n[0] > board.rows or n[1] > board.cols or n[0] < 1 or n[1] < 1:
			pass
		elif board.markers[n[0] - 1][n[1] - 1] != 0:
			pass
		elif n in visited:
			pass
		else:
			visited.append(n)
			newNeighbors = getNeighbors(n)
			for nn in newNeighbors:
				toCheck.append(nn)
			
	return False
			

def checkConnectivity(boards, paths):
	connectivities = []
	
	for i in range(0, len(boards)):
		checkPaths(paths[i])
		
		points = boards[i].points
		currentConnectivities = []
		
		for p in range(0, len(points), 2):
			if points[p].status != 1:
				posA = (points[p].row, points[p].col)
				posB = (points[p + 1].row, points[p + 1].col)
				if checkReachability(boards[i], posA, [], posB) == False:
					points[p].status = 3
					points[p + 1].status = 3
		
		for n in range(0, len(boards[i].points), 2):
			connectivities.append(boards[i].points[n].status)
		
	return lstToStr(connectivities)
