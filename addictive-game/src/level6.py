from classes import *
from helper import *
from level4 import *

def findSurePaths(boards, paths):
	allPaths = []
	allPaths[0] = len(boards)
	allPaths[1] = 0
	
	for i in range(0, len(boards)):
		checkPaths(paths[i])
		
		surePaths = []
		partialPaths = []
		
		for p in boards[i].points:
			if p.status == 2:
				openPos.append(p)
		
		while !done:
			done = True
			
			for pp in partialPaths[0]:
				# if pp is not a path
				neighbors = getNeighbors((pp[len(pp) - 1].row, pp[len(pp) - 1].col))
				
				for n in neighbors:
					if n in pp:
						neighbors.remove(n)
					
					marker = boards[i].markers[n[0] - 1][n[1] - 1]
					if n[0] - 1 >= boards[i].rows or n[1] - 1 >= boards[i].cols:
						neighbors.remove(n)
					elif abs(marker) != p.color and marker != 0:
						neighbors.remove(n)
					
				if len(neighbors) == 1:
					done = False
					pp.append(neighbours[0][0], neighbours(0][1])
					for c in partialPaths:
						if neighbours[0] == c[len(c) - 1)]:
							newPath = pp.mergePaths(c)
							partialPaths.remove(pp)
							partialPaths.remove(c)
							break
							
		# append num of paths and path
		# make sure order is correct
			
		for p in surePaths:
			p = str(p)
	
	return lstToStr(surePaths)
