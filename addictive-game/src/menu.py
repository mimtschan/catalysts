from classes import *
from level1 import *
from level2 import *
from level3 import *
from level4 import *
from level5 import *
from level6 import *

def menu(filename, board, paths, action):
	print("input from " + filename)
	if action == 1:
		return getPositions(board)
	elif action == 2:
		return getDistances(board)
	elif action == 3:
		return checkPaths(paths)
	elif action == 4:
		return drawBoard(filename, board, paths)
	elif action == 5:
		return checkConnectivity(board, paths)
	elif action == 6:
		return findSurePaths(board, paths)
	else:
		return "not a valid action"
