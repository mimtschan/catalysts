class Bid:
	def __init__(self, price, bidder):
		self.price = int(price)
		self.bidder = bidder
	
	def __str__(self):
		return self.bidder + "," + str(self.price)
		
class Auction:
	def __init__(self, startPrice, buyNow, bids):
		self.currentPrice = int(startPrice)
		self.maxBid = Bid(startPrice, None)
		self.bids = []
		self.history = "-," + str(startPrice)
		self.buyNow = int(buyNow)
		
		print "Starting offer: " + startPrice
		print "Buy Now Price: ",
		if self.buyNow > 0:
			print buyNow
		else:
			print "none"
		print "Bids:          ",
		
		for n in range(0, len(bids), 2):
			newBid = Bid(bids[n + 1], bids[n])
			self.bids.append(newBid)
			
		
	def addToHistory(self, bid):
		self.history += "," + str(bid)
	
	def applyBid(self):
		nextBid = self.bids[0]
		sameBidder = False
		
		self.bids.remove(nextBid)
	
		if not self.maxBid.bidder:
			self.maxBid.bidder = nextBid.bidder
			self.maxBid.price = nextBid.price

		else:			
			if nextBid.bidder == self.maxBid.bidder:
				sameBidder = True
	
			if nextBid.price > self.maxBid.price:
				if not sameBidder:
					self.currentPrice = self.maxBid.price + 1
					self.maxBid.bidder = nextBid.bidder
				self.maxBid.price = nextBid.price
			elif nextBid.price < self.maxBid.price:
				self.currentPrice = nextBid.price + 1
			else:
				self.currentPrice = nextBid.price
		
		if self.buyNow > 0 and self.currentPrice >= self.buyNow:
			self.currentPrice = self.buyNow
			self.bids = []
		
		if not sameBidder:
			self.addToHistory(Bid(self.currentPrice, self.maxBid.bidder))
		
		
	def runAuction(self):
		while len(self.bids) > 0:
			self.applyBid()
		if self.maxBid.bidder == None:
			return
		else:
			self.maxBid.price = self.currentPrice
			print self.history
