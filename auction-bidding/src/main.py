import sys
from auction import *

# helper functions

def strToLst(string):
	return string.split(",")
	
def lstToStr(lst):
	return ",".join(lst)

def setup():
	inp = strToLst(sys.argv[1])
	start = inp[0]
	buyNow = inp[1]
	
	inp.remove(start)
	inp.remove(buyNow)
		
	auction = Auction(start, buyNow, inp)
	auction.runAuction()

setup()
