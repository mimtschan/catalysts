import sys

def printImage(image):
	for i in image:
		print i

def createImage(width, height, inp):
	image = []
	nextRow = []
	
	i = 0
	for r in range (0, height):
		for c in range(0, width):
			nextRow.append(int(inp[i]))
			i += 1
		image.append(nextRow)
		nextRow = []
		
	printImage(image)
	return image
	
	
def findCraters(image):
	x = -1
	for i in range(0, len(image[0])):
		for j in range(0, len(image)):
			if image[j][i] == 1:
				x = i - 1
				break
		if x >= 0:
			break
				
	for k in range(0, len(image)):
		if 1 in image[k]:
			y = k - 1
			break
			
	deltaX = None
	for i in range(len(image[0]) - 1, -1, -1):
		for j in range(len(image) - 1, -1, -1):
			if image[j][i] == 1:
				deltaX = i + 1 - x
				break
		if deltaX:
			break
	
	for k in range(len(image) - 1, -1, -1):
		if 1 in image[k]:
			deltaY = k + 1 - y
			break
	
	print str(x) + " " + str(y) + " " + str(deltaX) + " " + str(deltaY)
			
userInp = sys.argv[1]
userInp = userInp.split(" ")
userWidth = int(userInp[1])
userHeight = int(userInp[0])
userInp.remove(str(userWidth))
userInp.remove(str(userHeight))

userImg = createImage(userWidth, userHeight, userInp)
findCraters(userImg)
