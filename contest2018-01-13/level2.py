import sys

def printImage(image):
	for i in image:
		print i

def createImage(width, height, inp):
	image = []
	nextRow = []
	
	i = 0
	for r in range (0, height):
		for c in range(0, width):
			nextRow.append(int(inp[i]))
			i += 1
		image.append(nextRow)
		nextRow = []
		
	printImage(image)
	return image

def paintLuminosity(width, height, inp):
	lum = [0] * 8
	lumOffset = width * height
	numPairs = int(inp[lumOffset])

	for l in range(lumOffset + 1, lumOffset + numPairs * 2, 2):
		lum[int(inp[l])] = int(inp[l + 1])
	
	for i in range(0, len(inp)):
		inp[i] = lum[int(inp[i])]
	
	return createImage(width, height, inp)

def analyzeImage(image):
	visited = [False] * 8
	visited[0] = True
	
	for i in range(0, len(image)):
		currentLum = 0
		
		for j in range(0, len(image[0])):
			lum = image[i][j]
			if lum < currentLum and not visited[lum]:
				visited[lum] = True
				for r in range(len(image) - 1, i, -1):
					if (lum in image[r]) != (currentLum in image[r]):
						break
					return lum, currentLum
				currentLum = 0
			elif not visited[lum]:
				visited[lum] = True
				currentLum = lum
	
	return 0, 0
	
def pairToImage(a, b, image):
	for i in range(0, len(image)):
		for j in range(0, len(image[0])):
			if image[i][j] == a or image[i][j] == b:
				image[i][j] = 1
			else:
				image[i][j] = 0
				
	printImage(image)
	return image
	
def imgToStr(image):
	width = str(len(image))
	height = str(len(image[0]))
	
	for i in range(0, len(image)):
		image[i] = " ".join(str(s) for s in image[i])
	imageStr = " ".join(image)
	
	return width + " " + height + " " + imageStr

def findCraters(image):
	x = -1
	for i in range(0, len(image[0])):
		for j in range(0, len(image)):
			if image[j][i] == 1:
				x = i - 1
				break
		if x >= 0:
			break
				
	for k in range(0, len(image)):
		if 1 in image[k]:
			y = k - 1
			break
			
	deltaX = None
	for i in range(len(image[0]) - 1, -1, -1):
		for j in range(len(image) - 1, -1, -1):
			if image[j][i] == 1:
				deltaX = i + 1 - x
				break
		if deltaX:
			break
	
	for k in range(len(image) - 1, -1, -1):
		if 1 in image[k]:
			deltaY = k + 1 - y
			break
	
	print str(x) + " " + str(y) + " " + str(deltaX) + " " + str(deltaY)
	return x, y, deltaX, deltaY
			
userInp = sys.argv[1]
userInp = userInp.split(" ")
userWidth = int(userInp[1])
userHeight = int(userInp[0])
userInp.remove(str(userWidth))
userInp.remove(str(userHeight))

userImg = paintLuminosity(userWidth, userHeight, userInp)
a, b = analyzeImage(userImg)
print
userImg = pairToImage(a, b, userImg)
print
print imgToStr(userImg)
