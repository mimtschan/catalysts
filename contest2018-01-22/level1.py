import sys

# classes

class Nonogram:
	def __init__(self, cols, blocks):
		self.width = cols
		# later: self.height = rows
		self.grid = [0] * cols
		self.row = blocks
		
	def markRows(self):
		currentIndex = 0
		
		for block in self.row:
			for i in range(block):
				self.grid[currentIndex + i] += 1
			currentIndex += block + 1
		
		currentIndex = self.width - 1
		
		for b in range(len(self.row) - 1, -1, -1):
			for i in range(self.row[b]):
				self.grid[currentIndex - i] += 1
			currentIndex -= self.row[b] - 1
		
		return lstToStr(self.grid)
			
			
# helpers
	
def strToLst(inp):
	return [int(i) for i in inp.split(" ")]
		
def lstToStr(out):
	solution = ""
	
	for i in out:
		if i > 1:
			solution += "1"
		else:
			solution += "?"
			
	return solution

def setup(inp):
	inp = strToLst(inp)
	cols = inp[0]
	inp.remove(cols)
	return Nonogram(cols, inp)

# main

nonogram = setup(sys.argv[1])
print nonogram.markRows()
